<?php

return [
    'driver' => 'mysql',
    'host' => 'localhost',
    'db_name' => 'task_board',
    'db_user' => 'root',
    'db_password' => '',
    'charset' => 'utf8',
];
