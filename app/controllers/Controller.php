<?php


namespace app\controllers;
use app\views\View;

class Controller
{
    protected $model;
    protected $view;
    public function __construct($route){
        $model = 'app\models\\'.$route['controller'].'Model';
        if(class_exists($model)){
            $this->model = new $model();
        }
        $this->view = new View($route);
    }

}