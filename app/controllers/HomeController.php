<?php


namespace app\controllers;

use app\patterns\bind\A;
use app\patterns\bind\B;
use app\patterns\factory\Creator;
use app\patterns\factory\RoadWay;

class HomeController extends Controller
{
    public function actionIndex(){
        $this->view->render();
    }



    public function actionTestPatterns(){

       $this->view->render(B::selfName());
    }






}