<?php


namespace app\router;


class Router
{
    private $routes;
    private $pointers;
    private $parameters = null;

    public function __construct()
    {
        $fileRoutes = require __DIR__ .'/roterConfig.php';
        foreach ($fileRoutes as $route => $params){
            $this->add($route, $params);
        }
    }

    private function add($route, $params){
        $route = preg_replace('!{([a-z]+):([^\}]+)}!', '(?P<\1>\2)', $route);
        $route = '#^'.$route.'$#';
        $this->routes[$route] = $params;
    }

    private function checkRoute(){
        $uri = substr($_SERVER['REQUEST_URI'],1);
        foreach ($this->routes as $route => $params){
            if(preg_match($route, $uri, $match)){
                $this->pointers = $params;
                foreach ($match as $key => $match){
                    if(is_string($key)){
                        $this->parameters[$key] = $match;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function run(){
        if($this->checkRoute()){
            $controller = 'app\controllers\\' .$this->pointers['controller'] . 'Controller';
            if(file_exists($controller . '.php')){
                $controller = new $controller($this->pointers);
                if(method_exists($controller, 'action'.$this->pointers['action'])){
                    $action = 'action'.$this->pointers['action'];
                    if($this->parameters != null){
                        $controller->$action($this->parameters);
                    }else{
                        $controller->$action();
                    }
                }else{
                    echo '<br>404 метод не найден<br>';
                }
            }else{
                echo '<br> 404 - файл не найден <br>';
            }
        }else{
            echo '<br> 404 - такого маршута нету <br>';
        }
    }
}