<?php
/**
 * url =>
 * usl/{name:\RegularExpresion} - for parameters
 *example url/{id:\d+}
 *example url/{id:\d+}/{name:\w+} - same names for parameters - wrong! names for parameters contain only character
 */
return [
    '' => [
        'controller' => 'Home',
        'action' => 'Index',
    ],
];