<?php


namespace app\views;


class View
{
    private $directory = 'default';
    private $layout = 'default';
    public function __construct($route){
        $this->directory = lcfirst($route['controller']);
        $this->layout = lcfirst($route['action']);
    }

    public function render($valueFromController = null){
        $path = __DIR__ .'/' . $this->directory . '/'.$this->layout .'.php';
        if(file_exists($path)){
            require_once $path;
        }else{
            echo '<br>Файл видов не найден<br>';
        }
    }



}