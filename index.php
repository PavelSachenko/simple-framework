<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once 'app/config/start.php';

use app\router\Router;

$route = new Router();
$route->run();
